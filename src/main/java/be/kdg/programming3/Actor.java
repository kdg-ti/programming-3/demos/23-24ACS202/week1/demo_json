package be.kdg.programming3;

import com.google.gson.annotations.SerializedName;

import java.util.Random;

public class Actor {
    public enum Gender {
        M, F, X
    }
    private String name;
    private Gender gender;
    @SerializedName("birth_year")
    private int birthyear;

    public Actor(String name, Gender gender, int birthyear) {
        this.name = name;
        this.gender = gender;
        this.birthyear = birthyear;
    }
    public String getName() {
        return name;
    }
    public Gender getGender() {
        return gender;
    }
    public int getBirthyear() {
        return birthyear;
    }
    @Override
    public String toString() {
        return "Actor{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", birthyear=" + birthyear +
                '}';
    }

    public static Actor createRandomActor() {
        Random random = new Random();
        int birthyear = random.nextInt(1920, 2010);
        Gender gender = Gender.values()[random.nextInt(3)];
        String name = "Sam" + random.nextInt(100);
        return new Actor(name, gender, birthyear);
    }
}
