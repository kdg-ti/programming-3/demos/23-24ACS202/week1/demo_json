package be.kdg.programming3;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.stream.Stream;

public class StartApplication {
    public static void main(String[] args) {
        List<Actor> actors = Stream.generate(Actor::createRandomActor)
                .limit(10)
                .toList();
        //Let's convert this list to a json file!
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        String jsonString = gson.toJson(actors);
        System.out.println(jsonString);
    }
}
